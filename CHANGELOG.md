# Changelog

## 1.5.1 (17 June 2021)

* Removed: Silver Arrows

## 1.5.0 (13 June 2020)

* Fixed: Branding

## 1.4.3 (22 May 2020)

* Fixed: Incorrect file for NFF

## 1.4.2 (22 May 2020)

* Fixed: AIO install option

## 1.4.1 (14 May 2020)

* Added: NFF Consistency Patch
* Added: Skyrim Souls RE Patch
* Updated: Aetherium Swords and Armor Patch
* Updated: Inigo Consistency Patch
* Updated: Oblivion Artifacts Patch
* Updated: Skyrim Sewers Patch

## 1.4.0 (25 April 2020)

* Added: M'rissi Patch
* Updated: FOMOD structure

## 1.3.2 (19 April 2020)

* Added: DBM Safehouse Fireplace Fix
* Added: LOTD Safehouse KiC Bathroom Patch

## 1.3.1 (16 April 2020)

* Removed: Predator Vision

## 1.24 (09 April 2020)

* Updated: Legacy of the Dragonborn
* Updated: The Brotherhood of Old

## 1.21 (01 April 2020)

* Added: Silver Arrows
* Removed: Ruins Edge/Staff of Sheogorath
* Removed: Thane Weapons Reborn
* Removed: ImRevelation's Smithing Tweaks
* Updated: Black Sacrament Armor
* Updated: Faction Crossbows
* Updated: Lustmord Vampire Armor
* Updated: NorthGirl Armor
* Updated: Rogue Armor HD
* Updated: Scout Armor
* Updated: Talos Housecarl
* Updated: Volkihar Knight
* Updated: Wayfarers Coat SSE
* Updated: Aetherium Swords and Armor
* Updated: Lore Weapon Expansion SE
* Updated: Skyrim's Unique Treasures
* Updated: Zim's Immersive Artifacts
* Updated: OBIS SE
* Updated: Skyrim Immersive Creatures
* Updated: Song of the Green
* Updated: Legacy of the Dragonborn
* Updated: Falskaar
* Updated: Helgen Reborn
* Updated: Moon And Star
* Updated: Survival and Difficulty
* Updated: Predator Vision
* Updated: Omega Populated

## 1.15 (12 November 2019)

* OMEGA Populated Patch - Removed the 3 spiders added to Redbelly mine as they are still present even after you have been asked to clear the mine.
* OMEGA Populated Patch - Removed Bandits added to Silent Moon Camp.

## 1.14.1 (05 November 2019)

* Re-added accidentally removed Falskaar patch

## 1.14 (04 November 2019)

* Removed several patches now provided upstream

## 1.13.1 (23 October 2019)

* Remember not to patch while inebriated

## 1.13 (16 October 2019)

* Bring patches up to date

## 1.12.1 (26 September 2019)

* Initial upload
